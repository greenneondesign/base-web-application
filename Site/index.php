<?php
	/**
	 *	We use the .htaccess file in this same directory to route resource non-matches
	 *	here which has the effect of setting our include path and then pass control
	 *	to Application.php. This step could be bypassed through careful
	 *	configuration of PHP
	 *
	 *	If you are looking for the default page, you should look at the default_route
	 *	set in core/config.ini 
	 */
	include_once($_SERVER['DOCUMENT_ROOT'] . '/core/Application/Application.php');
?>